package com.zjc.mall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zjc.mall.common.utils.PageUtils;
import com.zjc.mall.member.entity.MemberLoginLogEntity;

import java.util.Map;

/**
 * 会员登录记录
 *
 * @author JinChen Zhu
 * @email 2224257273@qq.com
 * @date 2024-07-18 17:19:13
 */
public interface MemberLoginLogService extends IService<MemberLoginLogEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

