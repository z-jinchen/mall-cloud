package com.zjc.mall.member.dao;

import com.zjc.mall.member.entity.MemberEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员
 * 
 * @author JinChen Zhu
 * @email 2224257273@qq.com
 * @date 2024-07-18 17:19:14
 */
@Mapper
public interface MemberDao extends BaseMapper<MemberEntity> {
	
}
