package com.zjc.mall.member.feign;

import com.zjc.mall.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @Author: JinChen Zhu
 * @CreateTime: 2024-07-19  18:07
 * @Description: 声明式远程调用
 */
@FeignClient("mall-coupon") // 告诉spring cloud需要调用哪个远程服务
public interface CouponFeignService {
    @RequestMapping("/coupon/coupon/member/list") // 声明调用哪个接口
    R memberCoupon();
}
