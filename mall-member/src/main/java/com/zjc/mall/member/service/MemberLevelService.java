package com.zjc.mall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zjc.mall.common.utils.PageUtils;
import com.zjc.mall.member.entity.MemberLevelEntity;

import java.util.Map;

/**
 * 会员等级
 *
 * @author JinChen Zhu
 * @email 2224257273@qq.com
 * @date 2024-07-18 17:19:13
 */
public interface MemberLevelService extends IService<MemberLevelEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

