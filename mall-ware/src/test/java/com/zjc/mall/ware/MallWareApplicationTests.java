package com.zjc.mall.ware;

import com.zjc.mall.ware.service.impl.MinioServiceImpl;
import io.minio.MinioClient;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class MallWareApplicationTests {
    @Autowired
    private MinioClient minioClient;

    @Autowired
    private MinioServiceImpl minioService;
    @Test
    void contextLoads() {
    }

    @Test
    void testGetPresignedObjectUrl() throws Exception {
        String bucketName = "images";
        String objectName = "avatar01.jpg";
        minioService.getPresignedObjectUrl(bucketName, objectName);
        for (int i = 7; i < 20; i++) {
            objectName = "avatar" + i + ".jpg";
            minioService.getPresignedObjectUrl(bucketName, objectName);
        }


    }
}
