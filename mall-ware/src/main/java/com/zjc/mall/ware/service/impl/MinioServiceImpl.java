package com.zjc.mall.ware.service.impl;

import com.zjc.mall.ware.config.MinioConfig;
import com.zjc.mall.ware.service.MinioService;
import io.minio.*;
import io.minio.http.Method;
import io.minio.messages.Bucket;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: JinChen Zhu
 * @CreateTime: 2024-07-26  09:32
 */
@Service
@Slf4j
public class MinioServiceImpl implements MinioService {

    @Autowired
    private MinioClient minioClient;
    @Autowired
    private MinioConfig minioConfig;

    /**
     * 检查存储桶是否存在
     *
     * @param bucketName 桶名称
     * @return 布尔值
     */
    public boolean bucketExists(String bucketName) {
        try {
            return minioClient.bucketExists(BucketExistsArgs.builder().bucket(bucketName).build());
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 列出所有存储桶
     *
     * @return 存储桶列表
     */
    public List<Bucket> listBuckets() {
        try {
            return minioClient.listBuckets();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 创建存储桶
     *
     * @param bucketName 存储桶名
     */
    public void makeBucket(String bucketName) {
        try {
            minioClient.makeBucket(MakeBucketArgs.builder().bucket(bucketName).build());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 上传文件
     *
     * @param bucketName  存储桶名
     * @param objectName  对象名
     * @param filePath    临时的文件路径
     * @param contentType 文件类型
     */
    public boolean putObject(String bucketName, String objectName, String filePath, String contentType) {
        try {
            // 检查文件大小
            Path path = Paths.get(filePath);
            if (Files.size(path) > minioConfig.getMaxFileSize())
                throw new IllegalArgumentException("文件大小超过限制");

            // 检查存储桶是否存在
            if (!bucketExists(bucketName))
                makeBucket(bucketName);

            // 上传文件
            minioClient.putObject(PutObjectArgs.builder()
                    .bucket(bucketName)
                    .object(objectName)
                    .stream(Files.newInputStream(path), Files.size(path), -1)
                    .contentType(contentType)
                    .build());
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 上传文件，并设置有效期
     *
     * @param bucketName  存储桶名
     * @param objectName  对象名
     * @param filePath    临时的文件路径
     * @param contentType 文件类型
     * @param expiration  有效期，单位为秒
     * @return 布尔值
     */
    public boolean putObject(String bucketName, String objectName, String filePath, String contentType, int expiration) {
        try {
            // 检查文件大小
            Path path = Paths.get(filePath);
            if (Files.size(path) > minioConfig.getMaxFileSize())
                throw new IllegalArgumentException("文件大小超过限制");

            // 检查存储桶是否存在
            if (!bucketExists(bucketName))
                makeBucket(bucketName);

            // 上传文件
            minioClient.putObject(PutObjectArgs.builder()
                    .bucket(bucketName)
                    .object(objectName)
                    .stream(Files.newInputStream(path), Files.size(path), -1)
                    .contentType(contentType)
                    .userMetadata(Collections.singletonMap("x-amz-expires", String.valueOf(expiration)))
                    .build());
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 下载文件
     *
     * @param bucketName 存储桶名
     * @param objectName 对象名
     * @return 字节数组
     */
    public byte[] getObject(String bucketName, String objectName) {
        try {
            InputStream inputStream = minioClient.getObject(GetObjectArgs.builder().bucket(bucketName).object(objectName).build());
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            // 将输入流转换为字节数组
            byte[] buffer = new byte[4096];
            int n = 0;
            while (-1 != (n = inputStream.read(buffer))) {
                outputStream.write(buffer, 0, n);
            }
            return outputStream.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 删除文件
     *
     * @param bucketName 存储桶名
     * @param objectName 对象名
     */
    public boolean removeObject(String bucketName, String objectName) {
        try {
            minioClient.removeObject(RemoveObjectArgs.builder().bucket(bucketName).object(objectName).build());
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

//    /**
//     * 检查指定的对象（文件）的状态
//     *
//     * @param bucketName
//     * @param objectName
//     * @return
//     */
//    public StatObjectResponse statObject(String bucketName, String objectName) {
//        try {
//            return minioClient.statObject(StatObjectArgs.builder().bucket(bucketName).object(objectName).build());
//        } catch (Exception e) {
//            e.printStackTrace();
//            return null;
//        }
//    }
//

    /**
     * 生成URL,以便可以通过HTTP访问
     *
     * @param bucketName 存储桶名
     * @param objectName 对象名
     * @return url字符串
     */
    public String getObjectUrl(String bucketName, String objectName) {
        try {
            return minioClient.getObjectUrl(bucketName, objectName);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public String getPresignedObjectUrl(String bucketName, String objectName) {
        Map<String, String> reqParams = new HashMap<>();
        reqParams.put("response-content-type", "application/json");
        String url;
        try {
            url = minioClient.getPresignedObjectUrl(
                    GetPresignedObjectUrlArgs.builder()
                            .method(Method.PUT) //这里必须是PUT，如果是GET的话就是文件访问地址了。如果是POST上传会报错.
                            .bucket(bucketName)
                            .object(objectName)
                            .expiry(60 * 60 * 24)
                            .extraQueryParams(reqParams)
                            .build());
            log.info(url); // 前端直传需要的url地址
            return url;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
