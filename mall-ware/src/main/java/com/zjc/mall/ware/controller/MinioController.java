package com.zjc.mall.ware.controller;

import com.zjc.mall.common.utils.R;
import com.zjc.mall.ware.service.MinioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @Author: JinChen Zhu
 * @CreateTime: 2024-07-26  10:33
 */
@RestController
@RequestMapping("ware/minio")
public class MinioController {
    @Autowired
    private MinioService minioService;

    @GetMapping("/getSign")
    public R getSign(@RequestParam Map<String, Object> params) {
//        System.out.println(params);
        String bucketName = params.get("bucketName").toString();
        // 文件名
        String fileName = params.get("fileName").toString();
        return R.ok().put("data", minioService.getPresignedObjectUrl(bucketName, fileName));

    }
}
