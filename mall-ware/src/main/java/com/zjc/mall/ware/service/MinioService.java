package com.zjc.mall.ware.service;

import io.minio.messages.Bucket;

import java.util.List;

public interface MinioService {
    /**
     * 检查存储桶是否存在
     *
     * @param bucketName 桶名称
     * @return 布尔值
     */
    boolean bucketExists(String bucketName);

    /**
     * 列出所有存储桶
     *
     * @return 存储桶列表
     */
    List<Bucket> listBuckets();

    /**
     * 创建存储桶
     *
     * @param bucketName 存储桶名
     */
    void makeBucket(String bucketName);

    /**
     * 上传文件
     *
     * @param bucketName  存储桶名
     * @param objectName  对象名
     * @param filePath    临时的文件路径
     * @param contentType 文件类型
     */
    boolean putObject(String bucketName, String objectName, String filePath, String contentType);

    /**
     * 上传文件，并设置过期时间
     *
     * @param bucketName  存储桶名
     * @param objectName  对象名
     * @param filePath    临时文件路径
     * @param contentType 文件类型
     * @param expiration  过期时间（秒）
     * @return 布尔值
     */
    boolean putObject(String bucketName, String objectName, String filePath, String contentType, int expiration);

    /**
     * 从存储桶中获取对象（文件）
     *
     * @param bucketName 存储桶名
     * @param objectName 对象名
     * @return 文件的字节数组
     */
    byte[] getObject(String bucketName, String objectName);

    /**
     * 删除存储桶中的对象（文件）
     *
     * @param bucketName 存储桶名
     * @param objectName 对象名
     */
    boolean removeObject(String bucketName, String objectName);

    /**
     * 检查指定的对象（文件）的状态
     *
     * @param bucketName 存储桶名
     * @param objectName 对象名
     * @return StatObjectResponse对象
     */
//    StatObjectResponse statObject(String bucketName, String objectName);

    /**
     * 生成签名URL，以便可以通过HTTP访问
     *
     * @param bucketName 存储桶名
     * @param objectName 对象名
     * @return URL字符串
     */
    String getObjectUrl(String bucketName, String objectName);


    public String getPresignedObjectUrl(String bucketName, String objectName);
}
