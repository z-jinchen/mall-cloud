package com.zjc.mall.ware.dao;

import com.zjc.mall.ware.entity.PurchaseEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 采购信息
 * 
 * @author JinChen Zhu
 * @email 2224257273@qq.com
 * @date 2024-07-18 17:43:09
 */
@Mapper
public interface PurchaseDao extends BaseMapper<PurchaseEntity> {
	
}
