package com.zjc.mall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zjc.mall.common.utils.PageUtils;
import com.zjc.mall.ware.entity.WareInfoEntity;

import java.util.Map;

/**
 * 仓库信息
 *
 * @author JinChen Zhu
 * @email 2224257273@qq.com
 * @date 2024-07-18 17:43:09
 */
public interface WareInfoService extends IService<WareInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

