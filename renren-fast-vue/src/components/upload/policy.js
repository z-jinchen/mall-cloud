import http from '@/utils/httpRequest.js'
export function policy(params) {
  return new Promise((resolve, reject) => {
    http({
      url: http.adornUrl("/ware/minio/getSign"),
      method: "get",
      params: http.adornParams({ bucketName: params.bucketName, fileName: params.fileName })
    }).then(({ data }) => {
      resolve(data);
    })
  });
}
