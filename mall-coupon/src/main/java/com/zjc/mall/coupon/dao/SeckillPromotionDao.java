package com.zjc.mall.coupon.dao;

import com.zjc.mall.coupon.entity.SeckillPromotionEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 秒杀活动
 * 
 * @author JinChen Zhu
 * @email 2224257273@qq.com
 * @date 2024-07-18 16:48:13
 */
@Mapper
public interface SeckillPromotionDao extends BaseMapper<SeckillPromotionEntity> {
	
}
