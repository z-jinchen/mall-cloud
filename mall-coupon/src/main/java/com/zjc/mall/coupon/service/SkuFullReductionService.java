package com.zjc.mall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zjc.mall.common.utils.PageUtils;
import com.zjc.mall.coupon.entity.SkuFullReductionEntity;

import java.util.Map;

/**
 * 商品满减信息
 *
 * @author JinChen Zhu
 * @email 2224257273@qq.com
 * @date 2024-07-18 16:48:13
 */
public interface SkuFullReductionService extends IService<SkuFullReductionEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

