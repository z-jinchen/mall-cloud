package com.zjc.mall.product.dao;

import com.zjc.mall.product.entity.BrandEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 品牌
 * 
 * @author JinChen Zhu
 * @email 2224257273@qq.com
 * @date 2024-07-16 19:23:40
 */
@Mapper
public interface BrandDao extends BaseMapper<BrandEntity> {
	
}
