package com.zjc.mall.product.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zjc.mall.common.utils.PageUtils;
import com.zjc.mall.common.utils.Query;
import com.zjc.mall.product.dao.CategoryDao;
import com.zjc.mall.product.entity.CategoryEntity;
import com.zjc.mall.product.service.CategoryService;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;


@Service("categoryService")
public class CategoryServiceImpl extends ServiceImpl<CategoryDao, CategoryEntity> implements CategoryService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<CategoryEntity> page = this.page(
                new Query<CategoryEntity>().getPage(params),
                new QueryWrapper<CategoryEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<CategoryEntity> listWithTree() {
        // 1.查询所有分类
        List<CategoryEntity> allCategories = baseMapper.selectList(null);

        // 2.1 查找并组装一级分类（父分类ID为0）及其子分类
        List<CategoryEntity> topLevelCategories = allCategories.stream()
                // 筛选出父分类ID为0的一级分类
                .filter(category -> category.getParentCid() == 0)
                // 为每个一级分类设置其子分类
                .peek(category -> category.setChildren(getChildren(category, allCategories)))
                // 对一级分类按排序值进行排序，null值放在最后
                .sorted(Comparator.comparing(CategoryEntity::getSort, Comparator.nullsLast(Comparator.naturalOrder())))
                // 收集结果为列表
                .collect(Collectors.toList());

        return topLevelCategories; // 返回一级分类及其子分类的树形结构
    }

    /**
     * 递归获取一个分类的所有子分类
     *
     * @param parent        父分类实体
     * @param allCategories 所有分类的列表
     * @return 子分类的列表
     */
    private List<CategoryEntity> getChildren(CategoryEntity parent, List<CategoryEntity> allCategories) {
        return allCategories.stream()
                // 筛选出当前父分类的子分类
                .filter(category -> category.getParentCid().equals(parent.getCatId()))
                // 递归为每个子分类设置其子分类
                .peek(child -> child.setChildren(getChildren(child, allCategories)))
                // 对子分类按排序值进行排序，null值放在最后
                .sorted(Comparator.comparing(CategoryEntity::getSort, Comparator.nullsLast(Comparator.naturalOrder())))
                // 收集结果为列表
                .collect(Collectors.toList());
    }


    @Override
    public void removeMenuByIds(List<Long> list) {
        // TODO 检查是否有关联的子分类
        baseMapper.deleteBatchIds(list);
    }

    // 找到catelogId的完整路径，[父id,子id,孙id]，并且排好序
    @Override
    public Long[] findCatelogPath(Long catelogId) {
        // 创建一个空的列表用于存储路径
        List<Long> paths = new ArrayList<>();

        // 调用findParentPath方法查找父路径，并将结果存入parentPaths列表中
        List<Long> parentPaths = findParentPath(catelogId, paths);

        // 将parentPaths列表反转，因为是从子到父的顺序，反转后变为从父到子的顺序
        Collections.reverse(parentPaths);

        // 将parentPaths列表转换为数组并返回
        return paths.toArray(new Long[parentPaths.size()]);
    }


    private List<Long> findParentPath(Long catelogId, List<Long> paths) {
        // 将当前分类ID添加到路径列表中
        paths.add(catelogId);
        // 根据分类ID获取对应的分类实体
        CategoryEntity categoryEntity = this.getById(catelogId);
        // 如果分类实体存在父分类ID且不为0
        if (categoryEntity.getParentCid() != 0) {
            // 递归调用findParentPath方法，传入父分类ID和路径列表
            findParentPath(categoryEntity.getParentCid(), paths);
        }
        // 返回路径列表
        return paths;
    }

}