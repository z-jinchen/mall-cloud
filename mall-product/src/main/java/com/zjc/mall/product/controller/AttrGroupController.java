package com.zjc.mall.product.controller;

import com.zjc.mall.common.utils.PageUtils;
import com.zjc.mall.common.utils.R;
import com.zjc.mall.product.entity.AttrGroupEntity;
import com.zjc.mall.product.service.AttrGroupService;
import com.zjc.mall.product.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Map;



/**
 * 属性分组
 *
 * @author JinChen Zhu
 * @email 2224257273@qq.com
 * @date 2024-07-16 19:23:41
 */
@RestController
@RequestMapping("product/attrgroup")
public class AttrGroupController {
    @Autowired
    private AttrGroupService attrGroupService;
    @Autowired
    private CategoryService categoryService;
    /**
     * 列表
     */
    @GetMapping("/list/{catelogId}")
    public R list(@RequestParam Map<String, Object> params, @PathVariable Long catelogId) {
//        PageUtils page = attrGroupService.queryPage(params);
        PageUtils page = attrGroupService.queryPage(params, catelogId);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @GetMapping("/info/{attrGroupId}")
    public R info(@PathVariable("attrGroupId") Long attrGroupId){
        // 通过属性组ID获取属性组实体
		AttrGroupEntity attrGroup = attrGroupService.getById(attrGroupId);
        // 获取属性组所属的分类ID
        Long catelogId = attrGroup.getCatelogId();
        // 根据分类ID获取分类路径
        Long[] catelogPath = categoryService.findCatelogPath(catelogId);
        // 设置属性组的分类路径
        attrGroup.setCatelogPath(catelogPath);
        // 返回属性组信息
        return R.ok().put("attrGroup", attrGroup);
    }


    /**
     * 保存
     */
    @PostMapping("/save")
    public R save(@RequestBody AttrGroupEntity attrGroup){
		attrGroupService.save(attrGroup);

        return R.ok();
    }

    /**
     * 修改
     */
    @PutMapping("/update")
    public R update(@RequestBody AttrGroupEntity attrGroup){
		attrGroupService.updateById(attrGroup);

        return R.ok();
    }

    /**
     * 删除
     */
    @DeleteMapping("/delete")
    public R delete(@RequestBody Long[] attrGroupIds){
		attrGroupService.removeByIds(Arrays.asList(attrGroupIds));

        return R.ok();
    }

}
