package com.zjc.mall.product;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zjc.mall.product.entity.BrandEntity;
import com.zjc.mall.product.service.BrandService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
@Slf4j
class MallProductApplicationTests {
    @Autowired
    private BrandService brandService;
    // 测试添加品牌
    @Test
    void testSaveBrand() {
        BrandEntity brand = new BrandEntity();
        brand.setName("华为");
        boolean save = brandService.save(brand);
        log.info(String.valueOf(brand));
        log.info("保存成功{}", save);
    }

    // 测试查询品牌列表
    @Test
    void testQueryBrandList() {
        List<BrandEntity> list = brandService.list(new QueryWrapper<BrandEntity>().ge("brand_id", 1L));
        list.forEach(item -> log.info(item.toString()));
    }

    @Test
    void testUpload(){

    }
}
