package com.zjc.mall.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zjc.mall.common.utils.PageUtils;
import com.zjc.mall.order.entity.PaymentInfoEntity;

import java.util.Map;

/**
 * 支付信息表
 *
 * @author JinChen Zhu
 * @email 2224257273@qq.com
 * @date 2024-07-18 17:37:58
 */
public interface PaymentInfoService extends IService<PaymentInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

