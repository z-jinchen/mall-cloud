package com.zjc.mall.order.dao;

import com.zjc.mall.order.entity.OrderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单
 * 
 * @author JinChen Zhu
 * @email 2224257273@qq.com
 * @date 2024-07-18 17:37:59
 */
@Mapper
public interface OrderDao extends BaseMapper<OrderEntity> {
	
}
