package com.zjc.mall.order.dao;

import com.zjc.mall.order.entity.OrderItemEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单项信息
 * 
 * @author JinChen Zhu
 * @email 2224257273@qq.com
 * @date 2024-07-18 17:37:58
 */
@Mapper
public interface OrderItemDao extends BaseMapper<OrderItemEntity> {
	
}
