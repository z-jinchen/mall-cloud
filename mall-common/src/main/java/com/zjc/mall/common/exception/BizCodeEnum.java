package com.zjc.mall.common.exception;

import lombok.Getter;

/**
 * @Author: JinChen Zhu
 * @CreateTime: 2024-07-31  00:24
 */
@Getter
public enum BizCodeEnum {
    UNKNOWN_EXCEPTION(10000, "未知异常"),
    VALID_EXCEPTION(10001, "参数校验异常");
    //    TOCKEN_EXPIRE_EXCEPTION(10003, "token过期异常"),
//    LOGIN_ACCT_PASSWORD_EXCEPTION(10004, "账号或密码错误"),
    private int code;
    private String msg;

    BizCodeEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
